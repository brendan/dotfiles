# dotfiles

## New Setup
These files predate 2024. I'm now using Nix to manage my dotfiles: https://gitlab.com/brendan/nix

These are my personal dotfiles. Use at your own risk :wink:

## Usage
* Clone with `git clone git@gitlab.com:brendan/dotfiles.git ~/.dotfiles`
* Symlink all the files to ~/
* This can be automated with https://gitlab.com/brendan/formation