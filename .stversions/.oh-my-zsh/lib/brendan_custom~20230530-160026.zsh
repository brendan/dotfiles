alias finder='ofd'
alias backup='$HOME/repos/personal/next-mac/dotfiles/auto-save-bash.sh'
alias restart_bluetooth='$HOME/repos/personal/next-mac/dotfiles/restart_bluetooth.sh'

alias reset='clear && source ~/.zshrc'
alias cs='curl cht.sh/$1'
alias path="echo $PATH | tr : '\n'"
alias serve="python2 -m SimpleHTTPServer 8000"
alias nodeserve="npx http-server"
alias sshu="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $1"
alias devgl='ssh -i "~/.ssh/customer-success.pem" ubuntu@ec2-52-18-102-13.eu-west-1.compute.amazonaws.com'

alias serveo='ssh -R 80:localhost:$1 serveo.net'

alias clip='clipcopy $1'

alias upstream='ggsup'
alias setupstream='ggsup'
alias addaltssh="git remote add altssh $(git remote get-url origin | sed 's/@gitlab.com/@altssh.gitlab.com/')"

alias hn='hncli'
alias hnn='hncli next'
alias hno='hncli open'
alias hnc='hncli comments'
alias hnr='hncli resetr'



cat ~/pd_words

###
# GitLab `lab` CLI section
###

alias ci='lab ci view'
alias traceci='lab ci trace'
alias lint='lab ci lint'

###
# GitLab repo stuff
###
alias testwww='bundle exec rake lint'
alias mountgl='sudo mount -o resvport,rw -t nfs 10.0.0.33:/home/brendan/repos /private/ub-repos'
alias unmountgl='sudo umount -f /private/ub-repos'

alias runnerdev='cd $GOPATH/src/gitlab.com/gitlab-org/gitlab-runner/'
# Python stuff
# alias python='python3'
# alias python2='/usr/bin/python'

###
# Brendan's CLI todo-er
###

#alias todo='cd ~/repos/personal/todo && lab issue list && cd -'

alias t='python ~/repos/opensource/t-gl/t.py --task-dir ~/repos/personal/tasks --list tasks'

alias sshpassword='ssh -o PreferredAuthentications=password -o PubkeyAuthentication=no $1'

export BAT_THEME='TwoDark'
export GITLAB_API_ENDPOINT=https://gitlab.com/api/v4

FILE=~/.gitlab-token
if test -f "$FILE"; then
    export GITLAB_API_PRIVATE_TOKEN=$(cat ~/.gitlab-token)
    export GITLAB_TOKEN=$(cat ~/.gitlab-token)
fi
export AFTERSHIP_API_KEY=e64c3e67-9f26-449f-8c7f-1a62c28bfa50
export DOTNET_CLI_TELEMETRY_OPTOUT=1

export PATH="$GOPATH/bin:$PATH"